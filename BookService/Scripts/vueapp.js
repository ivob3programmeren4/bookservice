﻿// starturi's voor Web API Services
var booksUri = '/api/books/';
var authorsUri = '/api/authors/';

var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!'
    }
})

console.log('voor vm');


// test

var test = $.ajax({
    type: 'Get',
    url: booksUri,
    dataType: 'json',
    contentType: 'application/json',
}).success(function (data) {
    var self = this;

    // This way
    self.Books = data;

    // Or this way
    //vm.$set('totalItems', data);
}).error(function (jqXHR, textStatus, errorThrown) {
    console.log(JSON.stringify(errorThrown));
});




// end test
var vm = new Vue({
    //el: '#bookList',
    data: {
        Books: []
    },
    methods: {
        fetchData: function(){
            $.ajax({
                type: 'Get',
                url: booksUri,
                dataType: 'json',
                contentType: 'application/json',
            }).success(function (data) {
                var self = this;

                // This way
                self.Books = data;

                // Or this way
                //vm.$set('totalItems', data);
            }).error(function (jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(errorThrown));
            });
        }
    }
    //,
    //data: { Books: [] }
    //,
    //ready: function () {
    //    self = this;
    //    console.log("ajax");
    //    ajaxHelper(booksUri, 'GET').done(function (data) {         
    //        self.Books = data;
    //    });
    //}
})

console.log('na vm');

// hulpmethode voor het aanspreken van de Service, past indien nodig de errormelding aan.
ajaxHelper = function (uri, method, data) {
    return $.ajax({
        type: method,
        url: uri,
        dataType: 'json',
        contentType: 'application/json',
        data: data ? JSON.stringify(data) : null
    }).error(function (jqXHR, textStatus, errorThrown) {
        console.log(JSON.stringify(errorThrown));
    });
}


getBookDetail = function (item) {

    ajaxHelper(booksUri + item.Id(), 'GET').done(function (data) {
        
 
    });
}




getAuthors = function () {
    ajaxHelper(authorsUri, 'GET').done(function (data) {
        
    });
}






