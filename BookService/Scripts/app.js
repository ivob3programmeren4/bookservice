﻿// starturi's voor Web API Services
var booksUri = '/api/books/';
var authorsUri = '/api/authors/';


var BooksViewModel = function () {
    // Knockout werkt volgens MVVM:
    // Model: de dataklassen: voor ons de DTO-klassen, komen als JSON objecten uit de Web API
    // View: de GUI in html met databinding - indexKO.cshtml
    // ViewModel: Javascript object met de verschillende Models om het View te vullen + behaviourmethoden (cfr. Controllers), dit object hier :)

    // standaardwerkwijze Knockout.js: var self = this;
    // we hebben nu steeds een verwijzing naar het ViewModel in self, this kan veranderen van betekenis
    var self = this;

    // mogelijke foutmelding, bindbaar aan de UI, dus een observable()
    self.error = ko.observable();

    // isEdit: observable om te testen of we in editeermodus of toevoegmodus zijn
    self.isEdit = ko.observable(true);
    // actionDescription: computed observable, titel en knoptekst aanpassen naargelang de modus
    self.actionDescription = ko.computed(function () {
        if (self.isEdit()) return 'Edit book';
        else return 'Add Book';
    });

    //lijst met boekDTO's: de lijst met de basisgegevens voor de boeken
    self.books = ko.observableArray();

    // de gegevens van bookDTO observable maken: nodig wanneer een edit van een BookDetailDTO wordt omgezet in een BookDTO
    // zo wordt de GUI boeklijst aangepast bij editeren van een element uit de lijst van Array books
    // is niet nodig als de basisgegevens geladen worden met (*)
    self.Book = function (id, title, authorname) {
        this.Id = ko.observable(id),
        this.Title = ko.observable(title),
        this.AuthorName = ko.observable(authorname);
    }

    // lijst met AuteurDTO's: voor vullen combobox
    self.authors = ko.observableArray();

    // bookDTO: variabele om het BookDTO van de Array books te bewaren bij het selecteren van een boek uit de lijst
    // zo moeten we na een update de Arrays books niet opnieuw doorzoeken naar het juiste book
    // hoeft niet observable te zijn.
    self.bookDTO = undefined;

    // detailgegevens van het geslecteerde boek, zoals getoond in de GUI, dus observable
    self.bookDetail = ko.observable();

    // maak een nieuw BookDetailDTO-object aan: toevoegen van een nieuw boek
    self.getBookDetailNew = function (formElement) {
        var data = {
            Id: 0,
            Title: '',
            Genre: '',
            AuthorId: 1,
            AuthorName: '',
            Price: 0,
            Year: 0
        }
        self.bookDetail(data);
        self.isEdit(false); // niet in editeer- maar in toevoegmodus
    }

    // hulpmethode voor het aanspreken van de Service, past indien nodig de errormelding aan.
    self.ajaxHelper = function (uri, method, data) {
        self.error(''); // Foutmelding leegmaken
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).error(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown + ': ' + jqXHR.responseText); // Indien fout, foutmelding tonen
        });
    }

    // haal de detailgegevens van een boek op: BookDetailDTO
    // item wordt meegegeven vanuit de View als BookDTO, als BookDTO hier een observable is, dan vragen we het Id op met Id()
    self.getBookDetail = function (item) {
        self.bookDTO = item;    // het geselecteerde BookDTO veilig stellen in een variabele voor het geval van een update
        // zo moeten we de Array books na de update niet doorlussen

        self.ajaxHelper(booksUri + item.Id(), 'GET').done(function (data) {
            self.bookDetail(data);

            self.isEdit(true); // editeermodus
        });
    }



    // Haal de basisgegevens van de boeken: lijst van BookDTO's in de Array books
    self.getAllBooks = function () {
        self.ajaxHelper(booksUri, 'GET').done(function (data) {
            // om enkel te tonen is onderstaande statement genoeg (*)
            //self.books(data);
            // omdat we straks bookdata willen aanpassen (vanuit edit van een BookDetailDTO)
            // en we willen de gegevens in de lijst met Bookgegevens autmatisch laten aanpassen: alle properties een ko.observable()
            // elk object in data mappen we naar een object met de ko.utils.arrayMap hulpfunctie
            self.books(ko.utils.arrayMap(data, function (item) {
                return new self.Book(item.Id, item.Title, item.AuthorName);
            }));
        });
    }

    // Haal de gegevens van de auteurs op: AuthorDTO's in de Array Authors
    self.getAuthors = function () {
        self.ajaxHelper(authorsUri, 'GET').done(function (data) {
            self.authors(data);
        });
    }

    // Sla een boek op, afhankelijk van isEdit wordt dit een insert of een update
    self.saveBook = function (formElement) {
        // opgepast, het geupdate boek bevat enkel het geupdate Id van de Author, niet de Name!
        // dit is niet erg voor de update server-side (enkel AuthorId nodig voor update van Book), maar wel voor het updaten van de client-side boekenlijst 
        // onderstaande is eenvoudigst... maar heeft DOM manipulatie in ViewModel
        self.bookDetail().AuthorName = $('#selAuthor :selected').text();


        //zet ko-object om in echt JS-object: properties ipv functieaanroepen
        var saveBook = ko.toJS(self.bookDetail());

        if (self.isEdit()) {
            // update van een Book
            self.ajaxHelper(booksUri + self.bookDetail().Id, 'PUT', saveBook).done(function (item) {
                // Na de update van een Book BookDetailDTO de Array books (BookDTO) weer aanpassen
                // Hier doen we dus geen select-query op de db van de boekenlijst maar passen de Array aan, deze is observable, de GUI wijzigt mee.
                self.bookDTO.Title(item.Title);
                self.bookDTO.AuthorName(item.AuthorName);
            });
        } else {
            // insert van een Book
            self.ajaxHelper(booksUri, 'POST', saveBook).done(function (item) {
                var book = new self.Book(item.Id, item.Title, item.AuthorName);
                self.books.push(book);
            });
        }
    }

    // verwijder een boek
    self.deleteBook = function (item) {
        // delete van een Book
        self.ajaxHelper(booksUri + item.Id(), 'DELETE').done(function (data) {
            self.books.remove(item);
        });
    }



    // initiele data ophalen: de resp. functies uitvoeren
    self.getAllBooks();
    self.getAuthors();

};

// ViewModel koppelen aan de View
ko.applyBindings(new BooksViewModel());